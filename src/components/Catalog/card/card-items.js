import React from "react";
import {BsChevronDown, BsChevronUp, BsChevronRight} from "react-icons/bs"
import  './card-items.css'
import cap from '../photo/Cap.svg'
import {click} from "@testing-library/user-event/dist/click";

const CardItem = () => {
    return (
        <div className="Card-Items">
            <div className="Filter-card" >
                <BsChevronDown className='Compact-down' />
            </div>
            <div className="Card-list">
                <div className='Card'>
                    <div className='Card-img'>
                        <img src={cap} alt=""/>
                    </div>
                    <div className='Cap-title'>
                        <div className="Card-description">
                             <h4>Nike</h4>  {/* тут будет {title}*/} 
                            <p>French Fries Series</p> {/*тут {deccription}*/}
                        </div>
                        <div className="Cap-price">
                            <h4>4500c</h4> {/*тут {price} */}
                        </div>
                        
                    </div>
                </div>
                <div className='switches'>
                    <a href="">1</a>
                    <a href="">2</a>
                    <a href="">3</a>
                    <a href="">4</a>
                    <a href="">5</a>
                    <BsChevronRight className="switches-logo"/>
                </div>
            </div>
        </div>
    )
}

export default CardItem