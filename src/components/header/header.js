import React from 'react'
import './header.css'
import logo from '../photo/logo.svg'
import logo__basket from '../photo/logo__basket.svg'

const AppHeader = () => {
    return (
        <div className='App-header'>
            <div className='header__section'>
                <div className='header__item headerlogo'>
                    <a href='http://localhost:3001/'><img src={logo} alt='' /></a>
                </div>
                <span className='header__item headerButton'><a href=''>Каталог</a></span>
                <span className='header__item headerButton'><a href=''>Бренды</a></span>
                <span className='header__item headerButton'><a href=''>О нас</a></span>

                
            </div>
            <div className='header__section'>
                <div className='header__item'>
                        <form className='header__item input__search'>
                            <input type='text' className='search logo__loupe'/>
                        </form>
                </div> 
                <div className='header__item'>
                    <a href='' ><img src={logo__basket} alt=''  /></a>
                </div>
            </div>
            
        </div>
        
        
        

    )
}
export default AppHeader